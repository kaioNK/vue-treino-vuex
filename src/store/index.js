import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0, 
    personagens: [
      {id: 1, name: 'Ryu', country: 'Japan'},
      {id: 2, name: 'Ken', country: 'Ken'},
      {id: 3, name: 'Honda', country: 'Japan'},
      {id: 4, name: 'Blanka', country: 'Brazil'},
    ]
  },
  mutations: {
    INCREMENT(state){
      state.count++
    },
    INCREMENT_BY(state, n){
      state.count += n
    },
    INCREMENT_OBJECT(state, payload){
      state.count += payload.amount
    }
  },
  actions: {
    increment_action_async({commit}, time){
      setTimeout(() => {
        commit('INCREMENT')
      }, time)
    },
    increment_action_async_payload({commit}, payload){
      console.log(payload.time)
      setTimeout(() => {
        commit('INCREMENT')
      }, payload.time)
    }
  },
  modules: {
  },
  getters: {
    fromJapan: state => {
      return state.personagens.filter(p => p.country == 'Japan')
    },
    fromJapanLength: (state, getters) => {
      return getters.fromJapan.length
    },
    getPersonagemById: (state) => (id) => {
      return state.personagens.find(p => p.id === id)
    }
  }
})
